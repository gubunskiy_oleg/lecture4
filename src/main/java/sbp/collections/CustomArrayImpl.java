package sbp.collections;

import java.util.*;

/**
 * Class CustomArrayImpl represents a dynamic array with elements of any type
 *
 * @param <T> type of array
 */
public class CustomArrayImpl<T> implements CustomArray <T>{

    private T[] elements;
    private int size;

    /**
     *  class creator method
     */
    public CustomArrayImpl() {

        this.elements = (T[])new Object[10];
        this.size = 0;
    }

    /**
     * class creator method from collection
     * @param elements
     */
    public CustomArrayImpl(Collection<T> elements) {

        this(elements.size());
        elements.toArray(this.elements);
        this.size = elements.size();
    }

    /**
     * class creator method of capacity
     * @param capacity
     */
    public CustomArrayImpl(int capacity) {

        this.elements = (T[])new Object[capacity];
        this.size = 0;
    }

    /**
     * method size
     *
     * @return size of array
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * filled list array
     *
     * @return true if array empty, or false if array not empty
     */
    @Override
    public boolean isEmpty() {
        if (this.size == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add single item.
     *
     * @param item adding value
     */
    @Override
    public boolean add(T item) {
        if (Objects.nonNull(item)) {

            ensureCapacity(this.size + 1);
            this.elements[this.size++] = item;
            return true;
        } else {
            throw new IllegalArgumentException("parameter items is null");
        }
    }

    /**
     * Add all items.
     *
     * @param items array of adding values
     * @throws IllegalArgumentException if parameter items is null
     */
    @Override
    public boolean addAll(T[] items) {
        if (Objects.nonNull(items)) {

            ensureCapacity(this.size + items.length);
            System.arraycopy(this.elements, 0, this.elements, items.length, this.size);

            System.arraycopy(items, 0, this.elements, this.size,
                    items.length);
            this.size += items.length;
            return true;
        } else {
            throw new IllegalArgumentException("parameter items is null");
        }
    }

    /**
     * Add all items.
     *
     * @param items collection of adding values
     * @throws IllegalArgumentException if parameter items is null
     */
    @Override
    public boolean addAll(Collection<T> items) {

        if (!items.isEmpty()) {

            T[] temp = items.toArray((T[])new Object[items.size()]);
            return addAll(temp);
        } else {
            throw new IllegalArgumentException("parameter items is null");
        }
    }

    /**
     * Add items to current place in array.
     *
     * @param index - index
     * @param items - items for insert
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     * @throws IllegalArgumentException       if parameter items is null
     */
    @Override
    public boolean addAll(int index, T[] items) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException("index is out of bounds");
        }
        else if (Objects.nonNull(items)) {

            ensureCapacity(this.size + items.length);
            System.arraycopy(this.elements, index, this.elements, index + items.length,
                    this.size - index);

            System.arraycopy(items, 0, elements, index,
                    items.length);
            this.size += items.length;
            return true;
        } else {
            throw new IllegalArgumentException("parameter items is null");
        }
    }

    /**
     * Get item by index.
     *
     * @param index - index
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public T get(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index is out of bounds");
        } else {
            return elements[index];
        }
    }

    /**
     * Set item by index.
     *
     * @param index - index
     * @param item
     * @return old value
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public T set(int index, T item) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index is out of bounds");
        } else {
            T temp = this.elements[index];
            this.elements[index] = item;
            return temp;
        }
    }

    /**
     * Remove item by index.
     *
     * @param index - index of element
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public void remove(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index is out of bounds");
        } else {
            T[] temp = (T[])new Object[this.size - 1];
            for (int i = 0; i < index; i++)
            {
                temp[i] = this.elements[i];
            }
            for (int i = index; i < this.size - 1; i++)
            {
                    temp[i] = this.elements[i + 1];
            }
            this.elements = temp;
            this.size = this.size - 1;
        }
    }

    /**
     * Remove item by value. Remove first item occurrence.
     *
     * @param item - value by item
     * @return true if item was removed
     */
    @Override
    public boolean remove(T item) {
        int ind = indexOf(item);
        if (ind != -1)
        {
            remove(ind);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if item exists.
     *
     * @param item - item
     * @return true or false
     */
    @Override
    public boolean contains(T item) {
        for (int i = 0; i < this.size; i++) {

            if (this.elements[i].equals(item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Index of item.
     *
     * @param item - item
     * @return index of element or -1 of list doesn't contain element
     */
    @Override
    public int indexOf(T item) {
        for (int i = 0; i < this.size; i++) {

            if (this.elements[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Grow current capacity to store new elements if needed.
     *
     * @param newElementsCount - new elements count
     */
    @Override
    public void ensureCapacity(int newElementsCount) {
        if (newElementsCount > this.elements.length)
        {
            T[] temp = this.elements;
            int maxValue = this.size>newElementsCount?this.size:newElementsCount;
            this.elements = (T[])new Object[(maxValue * 3) / 2 + 1];
            System.arraycopy(temp, 0, this.elements, 0, this.size);
        }
    }

    /**
     * Get current capacity.
     */
    @Override
    public int getCapacity() {
        return this.elements.length;
    }

    /**
     * Reverse list. items in reverse order
     */
    @Override
    public void reverse() {

        for (int i = 0, j = this.size - 1; i < this.size / 2; i++, j--)
        {
            T tmp = this.elements[j];
            this.elements[j] = this.elements[i];
            this.elements[i] = tmp;
        }
    }

    /**
     * Get copy of current array.
     */
    @Override
    public Object[] toArray() {
        Object[] obj = new Object[this.size];
        System.arraycopy(this.elements, 0, obj, 0,
                this.size);
        return obj;
    }

    /**
     * For output to console
     *
     * @return String elements of array
     */

    public String toString()
    {
        String str = "";
        for (T el: elements) {
            if (el != null) {

                str += "[" + el.toString() + "] ";
            }
        }
        return str;
    }

    /**
     * Clear array
     */
    public void clear()
    {
        this.elements = (T[])new Object[]{};
        this.size = 0;
    }

    /**
     *  Trim capacity of array by size
     * @return
     */
    public void trimToSize()
    {
        T[] temp = this.elements;
        this.elements = (T[])new Object[size];
        System.arraycopy(temp, 0, this.elements, 0, this.size);
    }
}
