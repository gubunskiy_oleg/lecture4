package sbp.collections;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomArrayImplTest  {

    CustomArrayImpl <Integer> customImpl = new CustomArrayImpl<Integer>();

    /**
     * Testing method size
     */
    @Test
    public void testSize() {
        CustomArrayImpl customImpl = new CustomArrayImpl<>();
        customImpl.add(4);
        Assert.assertTrue(customImpl.size() >= 0);
        customImpl.add(15);
        Assert.assertTrue(customImpl.get(0) == (Integer)4);
        customImpl.add("33");
        Assert.assertFalse(customImpl.isEmpty());
        Assert.assertTrue(customImpl.size() == 3);
        Assert.assertTrue(customImpl.get(2) == "33");
    }

    /**
     * Testing method isEmpty
     */
    @Test
    public void isEmpty() {
        Assert.assertTrue(customImpl.isEmpty());
        customImpl.add(4);
        Assert.assertFalse(customImpl.isEmpty());
        customImpl.remove((Integer) 4);
        Assert.assertTrue(customImpl.isEmpty());
        customImpl.add(5);
        customImpl.add(78);
        customImpl.add(9);
        customImpl.remove(0);
        customImpl.remove(1);
        customImpl.remove(0);
        Assert.assertTrue(customImpl.isEmpty());
    }

    /**
     * Testing method add
     */
    @Test
    public void add() {
        CustomArrayImpl customImpl = new CustomArrayImpl<>();
        Assert.assertTrue(customImpl.add(4));
        Assert.assertTrue(customImpl.size() == 1);
        Assert.assertTrue(customImpl.add(15));
        Assert.assertTrue(customImpl.get(0) == (Integer)4);
        Assert.assertTrue(customImpl.add("33"));
        Assert.assertFalse(customImpl.isEmpty());
        Assert.assertTrue(customImpl.size() == 3);
        Assert.assertTrue(customImpl.get(2) == "33");
        Assertions.assertThrows(IllegalArgumentException.class,() -> customImpl.add(null));
    }

    /**
     * Testing method addAll
     * adding array
     */
    @Test
    public void testAddAll1() {
        Assert.assertTrue(customImpl.isEmpty());
        customImpl.add(45);
        Assert.assertTrue(customImpl.size() == 1);
        Integer[] arr = {8, 5, 3, 45, 8, 6};
        Assert.assertTrue(customImpl.addAll(arr));
        customImpl.add(12);
        Assert.assertTrue(customImpl.size() == 8);
        Assert.assertTrue(customImpl.get(5) == 8);
        Integer[] arr1 = null;
        Assertions.assertThrows(IllegalArgumentException.class,() -> customImpl.addAll(arr1));
    }

    /**
     * Testing method addAll
     * adding collection
     */
    @Test
    public void testAddAll2() {
        Assert.assertTrue(customImpl.isEmpty());
        customImpl.add(45);
        Integer[] arr = {8, 5, 3, 45, 8, 6};
        Assert.assertTrue(customImpl.addAll(Arrays.asList(arr)));
        customImpl.add(12);
        Assert.assertTrue(customImpl.size() == 8);
        Assert.assertTrue(customImpl.get(5) == 8);
        List <Integer> collection = new ArrayList<>();
        Assertions.assertThrows(IllegalArgumentException.class,() -> customImpl.addAll(collection));
    }

    /**
     * Testing method addAll
     * adding array from index
     */
    @Test
    public void testAddAll3() {
        Assert.assertTrue(customImpl.isEmpty());
        customImpl.add(45);
        Integer[] arr = {8, 5, 3, 45, 8, 6};
        Assert.assertTrue(customImpl.addAll(1, arr));
        customImpl.add(12);
        Assert.assertTrue(customImpl.size() == 8);
        Assert.assertTrue(customImpl.get(5) == 8);
        Integer[] arr1 = null;
        Assertions.assertThrows(IllegalArgumentException.class,() -> customImpl.addAll(1, arr1));
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> customImpl.addAll(9, arr));
    }

    /**
     * Testing method get
     * get from array list from index
     */
    @Test
    public void testGet() {
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> customImpl.get(1));
        customImpl.add(45);
        customImpl.add(4);
        Assert.assertTrue(customImpl.get(1) == 4);
    }

    /**
     * Testing method set,
     * setting value to index
     */
    @Test
    public void testSet() {
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> customImpl.set(1, 12));
        customImpl.add(7);
        customImpl.add(6);
        Assert.assertTrue(customImpl.set(1, 12) == 6);
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> customImpl.set(2, 5));
    }

    /**
     * Testing method remove,
     * remove from index
     */
    @Test
    public void testRemove1() {
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> customImpl.remove(1));
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        Assert.assertTrue(customImpl.size() == 3);
        customImpl.remove(1);
        Assert.assertTrue(customImpl.size() == 2);
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> customImpl.remove(2));
    }

    /**
     * Testing method remove,
     * remove by value
     */
    @Test
    public void testRemove2() {
        Assert.assertFalse(customImpl.remove((Integer) 6));
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        Assert.assertTrue(customImpl.size() == 3);
        Assert.assertTrue(customImpl.remove((Integer) 6));
        Assert.assertTrue(customImpl.size() == 2);
        Assert.assertFalse(customImpl.remove((Integer) 6));
        Assert.assertTrue(customImpl.remove((Integer) 15));
        Assert.assertFalse(customImpl.remove((Integer) 6));
        Assert.assertTrue(customImpl.remove((Integer) 7));
    }

    /**
     * Testing method contains,
     * contains value to list array
     */
    @Test
    public void testContains() {
        Assert.assertFalse(customImpl.contains((Integer) 6));
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        Assert.assertTrue(customImpl.contains((Integer) 6));
        Assert.assertFalse(customImpl.contains((Integer) 12));
        Assert.assertTrue(customImpl.contains((Integer) 15));
        Assert.assertFalse(customImpl.contains((Integer) 3));
        Assert.assertTrue(customImpl.contains((Integer) 7));
    }

    /**
     * Testing method indexOf,
     * return index of list array by value
     */
    @Test
    public void testIndexOf() {
        Assert.assertTrue(customImpl.indexOf((Integer) 6) == -1);
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        Assert.assertTrue(customImpl.indexOf((Integer) 6) == 1);
        Assert.assertTrue(customImpl.indexOf((Integer) 12) == -1);
        Assert.assertTrue(customImpl.indexOf((Integer) 15) == 2);
        Assert.assertTrue(customImpl.indexOf((Integer) 3) == -1);
        Assert.assertTrue(customImpl.indexOf((Integer) 7) == 0);
    }

    /**
     * Testing method reverse,
     * reverse of list array
     */
    @Test
    public void testReverse() {
        CustomArrayImpl <String> customImpl = new CustomArrayImpl<>();
        List <String> ci = new ArrayList<>();
        ci.add("(Integer) 1");
        ci.add("(Integer) 2");
        ci.add("(Integer) 3");
        ci.add("(Integer) 4");
        customImpl.addAll(ci);
        customImpl.reverse();
        for (int i = 0, j = ci.size() - 1; i < customImpl.size(); i++, j--)
        {
            Assert.assertTrue(customImpl.get(i) == ci.get(j));
        }
    }

    /**
     * Testing method toArray,
     * list array to array
     */
    @Test
    public void toArray() {
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        Assert.assertTrue(customImpl.toArray()[0].equals(7));
        Assert.assertTrue(customImpl.toArray()[1].equals(6));
        Assert.assertTrue(customImpl.toArray()[2].equals(15));
        Assert.assertTrue(customImpl.toArray().length == 3);
    }

    /**
     * Testing method toString,
     * output list array to string
     */
    @Test
    public void testToString() {
        customImpl.add(7);
        customImpl.add(6);
        customImpl.add(15);
        Assert.assertTrue(customImpl.toString().equals("[7] [6] [15] "));
    }
}